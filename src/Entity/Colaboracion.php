<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Table(name="colaboracion")
 */
class Colaboracion implements \JsonSerializable {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $idProyecto;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $idUsuario;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $idTarea;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdProyecto()
    {
        return $this->idProyecto;
    }

    /**
     * @param mixed $idProyecto
     */
    public function setIdProyecto($idProyecto): void
    {
        $this->idProyecto = $idProyecto;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario): void
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return mixed
     */
    public function getIdTarea()
    {
        return $this->idTarea;
    }

    /**
     * @param mixed $idTarea
     */
    public function setIdTarea($idTarea): void
    {
        $this->idTarea = $idTarea;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'idProyecto' => $this->getIdProyecto(),
            'idUsuario' => $this->getIdUsuario(),
            'idTarea' => $this->getIdTarea()
        ];
    }
}
