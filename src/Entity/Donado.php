<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity
* @ORM\Table(name="donado")
*/
class Donado implements \JsonSerializable
{
    /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $idProyecto;

    /**
     * @ORM\Column(type="string", length=100)
     *
     */
    private $usuarioDonador;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $cantidad;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdProyecto()
    {
        return $this->idProyecto;
    }

    /**
     * @param mixed $idProyecto
     */
    public function setIdProyecto($idProyecto): void
    {
        $this->idProyecto = $idProyecto;
    }

    /**
     * @return mixed
     */
    public function getUsuarioDonador()
    {
        return $this->usuarioDonador;
    }

    /**
     * @param mixed $usuarioDonador
     */
    public function setUsuarioDonador($usuarioDonador): void
    {
        $this->usuarioDonador = $usuarioDonador;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad): void
    {
        $this->cantidad = $cantidad;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'idProyecto' => $this->getIdProyecto(),
            'cantidad' => $this->getCantidad(),
            'usuarioDonador' => $this->getUsuarioDonador()
        ];
    }
}