<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Table(name="tarea")
 */
class Tarea implements \JsonSerializable {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $idProyecto;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     */
    private $colaboracion;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     *
     */
    private $idColaborador;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdProyecto()
    {
        return $this->idProyecto;
    }

    /**
     * @param mixed $idProyecto
     */
    public function setIdProyecto($idProyecto): void
    {
        $this->idProyecto = $idProyecto;
    }

    /**
     * @return mixed
     */
    public function getColaboracion()
    {
        return $this->colaboracion;
    }

    /**
     * @param mixed $colaboracion
     */
    public function setColaboracion($colaboracion): void
    {
        $this->colaboracion = $colaboracion;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion): void
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getIdColaborador()
    {
        return $this->idColaborador;
    }

    /**
     * @param mixed $idColaborador
     */
    public function setIdColaborador($idColaborador): void
    {
        $this->idColaborador = $idColaborador;
    }



/**
 * Specify data which should be serialized to JSON
 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
 * @return mixed data which can be serialized by <b>json_encode</b>,
 * which is a value of any type other than a resource.
 * @since 5.4.0
 */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'idProyecto' => $this->getIdProyecto(),
            'colaboracion' => $this->getColaboracion(),
            'descripcion' => $this->getDescripcion(),
            'idColaborador' => $this->getIdColaborador()
        ];
    }


}
