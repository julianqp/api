<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Table(name="proyecto")
 */
class Proyecto implements \JsonSerializable {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     *
     */
    private $usuario;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     */
    private $titulo;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     */
    private $donacion;

    /**
     * @ORM\Column(type="decimal")
     * @Assert\NotBlank()
     */
    private $donado;

    /**
     * @ORM\Column(type="decimal")
     * @Assert\NotBlank()
     */
    private $limiteDonado;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     */
    private $colaboracion;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     */
    private $descripcion;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     */
    private $sugerencias;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     */
    private $objetivos;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $puntuacion;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     */
    private $ambito;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank()
     *
     */
    private $cuenta;

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario): void
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getColaboracion()
    {
        return $this->colaboracion;
    }

    /**
     * @param mixed $colaboracion
     */
    public function setColaboracion($colaboracion): void
    {
        $this->colaboracion = $colaboracion;
    }

    /**
     * @return mixed
     */
    public function getObjetivos()
    {
        return $this->objetivos;
    }

    /**
     * @param mixed $objetivos
     */
    public function setObjetivos($objetivos): void
    {
        $this->objetivos = $objetivos;
    }

    /**
     * @return mixed
     */
    public function getSugerencias()
    {
        return $this->sugerencias;
    }
    /**
     * @param mixed $sugerencias
     */
    public function setSugerencias($sugerencias)
    {
        $this->sugerencias = $sugerencias;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getDonacion()
    {
        return $this->donacion;
    }
    /**
     * @param mixed $donacion
     */
    public function setDonacion($donacion)
    {
        $this->donacion = $donacion;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }
    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }
    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    /**
     * @return mixed
     */
    public function getDonado()
    {
        return $this->donado;
    }
    /**
     * @param mixed $donado
     */
    public function setDonado($donado)
    {
        $this->donado = $donado;
    }

    /**
     * @return mixed
     */
    public function getLimiteDonado()
    {
        return $this->limiteDonado;
    }

    /**
     * @param mixed $limiteDonado
     */
    public function setLimiteDonado($limiteDonado): void
    {
        $this->limiteDonado = $limiteDonado;
    }

    /**
     * @return mixed
     */
    public function getPuntuacion()
    {
        return $this->puntuacion;
    }

    /**
     * @param mixed $puntuacion
     */
    public function setPuntuacion($puntuacion): void
    {
        $this->puntuacion = $puntuacion;
    }

    /**
     * @return mixed
     */
    public function getAmbito()
    {
        return $this->ambito;
    }

    /**
     * @param mixed $ambito
     */
    public function setAmbito($ambito): void
    {
        $this->ambito = $ambito;
    }

    /**
     * @return mixed
     */
    public function getCuenta()
    {
        return $this->cuenta;
    }

    /**
     * @param mixed $cuenta
     */
    public function setCuenta($cuenta): void
    {
        $this->cuenta = $cuenta;
    }


    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            "titulo"=>$this->getTitulo(),
            "donacion"=>$this->getDonacion(),
            "donado"=>$this->getDonado(),
            "colaboracion"=>$this->getColaboracion(),
            "descripcion"=>$this->getDescripcion(),
            "sugerencias"=>$this->getSugerencias(),
            "objetivos"=>$this->getObjetivos(),
            "usuario"=>$this->getUsuario(),
            "puntuacion"=>$this->getPuntuacion(),
            "limiteDonado"=>$this->getLimiteDonado(),
            "ambito"=>$this->getAmbito(),
            "cuenta"=>$this->getCuenta()
            ];
    }
}