<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
* @ORM\Entity
* @ORM\Table(name="favorito")
*/
class Favorito implements \JsonSerializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $idProyecto;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $idColaborador;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdProyecto()
    {
        return $this->idProyecto;
    }

    /**
     * @param mixed $idProyecto
     */
    public function setIdProyecto($idProyecto): void
    {
        $this->idProyecto = $idProyecto;
    }

    /**
     * @return mixed
     */
    public function getIdColaborador()
    {
        return $this->idColaborador;
    }

    /**
     * @param mixed $idColaborador
     */
    public function setIdColaborador($idColaborador): void
    {
        $this->idColaborador = $idColaborador;
    }



    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'idProyecto' => $this->getIdProyecto(),
            'idColaborador' => $this->getIdColaborador()
        ];
    }
}