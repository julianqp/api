<?php
namespace App\Controller;

use App\Entity\Proyecto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Donado;
use App\Service\API;

/**
 * Donado controller.
 * @Route("/api", name="api_")
 */
class DonadoController extends Controller {

    /**
     *  Donacion nueva.
     * @Route("/donacion",
     *     methods={"POST"},
     *     name = "postDonacion")
     *
     * @return Response
     */
    public function postDonacion(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $donacion = new Donado();
        $donacion->setIdProyecto($data['idProyecto']);
        $donacion->setCantidad($data['cantidad']);
        $donacion->setUsuarioDonador($data['usuarioDonador']);


        $repository = $this->getDoctrine()->getRepository(Proyecto::class);
        $proyecto = $repository->findOneBy(['id'=> $data['idProyecto']]);
        if(!is_null($proyecto)){
            $manager->persist($donacion);
            $manager->flush();
            $donado = $proyecto->getDonado() + $data['cantidad'];
            $proyecto->setDonado($donado);
            $manager->merge($proyecto);
            $manager->flush();
        }

        return API::send_data($donacion);
    }
}