<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Colaboracion;
use App\Service\API;

/**
 * Usuario controller.
 * @Route("/api", name="api_")
 */
class ColaboracionController extends Controller
{
    /**
     * Lists all Colaboracioness.
     * @Route("/colaboraciones",
     *     methods = {"GET"},
     *     name = "getColaboraciones")
     *
     * @return Response
     */
    public function getColaboracionesAction()
    {
        $repository = $this->getDoctrine()->getRepository(Colaboracion::class);
        $colaboracion = $repository->findall();
        return API::send_data($colaboracion);
    }

    /**
     *  Info de una tarea.
     * @Route("/colaboraciones/{idProyecto}", methods={"GET"})
     * @param String idProyecto
     * @return Response
     */
    public function getColaboracion($idProyecto)
    {
        $repository = $this->getDoctrine()->getRepository(Colaboracion::class);
        $colaboracion = $repository->findBy(['$idProyecto' => $idProyecto]);
        return API::send_data($colaboracion);
    }

    /**
     *  Creating a new user.
     * @Route("/colaboraciones",
     *     methods={"POST"},
     *     name = "postColaboracion")
     *
     * @return Response
     */
    public function postColaboracionAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $colaboracion = new Colaboracion();
        $colaboracion->setIdProyecto($data['idProyecto']);
        $colaboracion->setIdUsuario($data['idUsuario']);
        $colaboracion->setIdTarea('idTarea');
        $manager->persist($colaboracion);
        $manager->flush();
        return API::send_data([]);
    }

    /**
     *  Deleting a tarea.
     * @Route("/colaboraciones/{idProyecto}",
     *     methods={"DELETE"},
     *     name = "deleteColaboraciones")
     * @param int idProyecto
     * @return Response
     */
    public function deleteColaboracionAction($idProyecto)
    {
        $manager = $this->getDoctrine()->getManager();
        $colaboracion = $this->getDoctrine()->getRepository(Colaboracion::class);
        $deleteColaboracion = $colaboracion->findBy(['idProyecto' => $idProyecto]);
        $manager->remove($deleteColaboracion);
        $manager->flush();

        return API::send_data([]);
    }
}
