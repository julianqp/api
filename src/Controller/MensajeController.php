<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Mensaje;
use App\Service\API;

/**
 * Mensaje controller.
 * @Route("/api", name="api_")
 */
class MensajeController extends Controller
{
    /**
     * Lists all Mensajes.
     * @Route("/mensajes",
     *     methods = {"GET"},
     *     name = "getMensajes")
     *
     * @return Response
     */
    public function getMensajesAction()
    {
        $repository = $this->getDoctrine()->getRepository(Mensaje::class);
        $mensaje = $repository->findall();
        return API::send_data($mensaje);
    }

    /**
     *  Info de una tarea.
     * @Route("/mensajes/{usuario}", methods={"GET"})
     * @param String usuario
     * @return Response
     */
    public function getMensaje($usuario)
    {
        $repository = $this->getDoctrine()->getRepository(Mensaje::class);
        $mensaje = $repository->findBy(['$usuario' => $usuario]);
        return API::send_data($mensaje);
    }

    /**
     *  Creating a new user.
     * @Route("/mensaje",
     *     methods={"POST"},
     *     name = "postMensaje")
     *
     * @return Response
     */
    public function postMensajeAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $mensaje = new Mensaje();
        $mensaje->setFecha($data['fecha']);
        $mensaje->setUsuario($data['usuario']);
        $mensaje->setDestinatario($data['destinatario']);
        $mensaje->setMensaje($data['mensaje']);

        $manager->persist($mensaje);
        $manager->flush();
        return API::send_data([]);
    }
}
