<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Proyecto;
use App\Service\API;
/**
 * Movie controller.
 * @Route("/api", name="api_")
 */
class ProyectoController extends Controller
{
    /**
     * Lists all Proyectos.
     * @Route("/proyectos",
     *     methods = {"GET"},
     *     name = "getProyectos")
     * @return Response
     */
    public function getProyctoAction()
    {
        $repository = $this->getDoctrine()->getRepository(Proyecto::class);
        $proyectos = $repository->findall();
       return API::send_data($proyectos);
    }

      /**
     *  Info de un proyecto.
     * @Route("/proyectos/{id}", methods={"GET"})
     * @param String id
     * @return Response
     */
    public function getProyecto($id)
    {
        $repository = $this->getDoctrine()->getRepository(Proyecto::class);
        $proyecto = $repository->findOneBy(['id'=>$id]);

        return API::send_data($proyecto);
    }

    /**
     *  Info de un proyecto.
     * @Route("/proyectos/usuarios/{usuario}", methods={"GET"})
     * @param String usuario
     * @return Response
     */
    public function getProyectosUsuario($usuario)
    {
        $repository = $this->getDoctrine()->getRepository(Proyecto::class);
        $proyecto = $repository->findBy(['usuario'=>$usuario]);

        return API::send_data($proyecto);
    }

    /**
     * Create Proycto.
     *  @Route("/proyecto",
     *     methods={"POST"},
     *     name = "postProyecto")
     * @return Response
     */
    public function postProyectoAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();
        $newProyecto = new Proyecto();

        $newProyecto->setTitulo($data['titulo']);
        $newProyecto->setDonacion($data['donacion']);
        $newProyecto->setDonado(0);
        $newProyecto->setColaboracion($data['colaboracion']);
        $newProyecto->setDescripcion($data['descripcion']);
        $newProyecto->setSugerencias($data['sugerencias']);
        $newProyecto->setObjetivos($data['objetivos']);
        $newProyecto->setAmbito($data['ambito']);
        $newProyecto->setPuntuacion($data['puntuacion']);
        $newProyecto->setLimiteDonado($data['limiteDonado']);
        $newProyecto->setUsuario($data['usuario']);
        $newProyecto->setCuenta($data['cuenta']);
        $manager->persist($newProyecto);
        $manager->flush();

        return API::send_data($newProyecto);
    }

    /**
     * editar Proyecto.
     *  @Route("/proyectos/{id}",
     *     methods={"POST"},
     *     name = "editProyecto")
     * @param int id
     * @return Response
     */
    public function postProyectoModificacion($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Proyecto::class);
        $proyecto = $repository->findOneBy(['id'=>$id]);

        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $proyecto->setTitulo($data['titulo']);
        $proyecto->setDonacion($data['donacion']);
        $proyecto->setColaboracion($data['colaboracion']);
        $proyecto->setDescripcion($data['descripcion']);
        $proyecto->setSugerencias($data['sugerencias']);
        $proyecto->setObjetivos($data['objetivos']);
        $proyecto->setAmbito($data['ambito']);
        $proyecto->setLimiteDonado($data['limiteDonado']);
        $proyecto->setCuenta($data['cuenta']);
        $manager->merge($proyecto);
        $manager->flush();
        return API::send_data($proyecto);
    }

    /**
     *  Deleting a proyecto.
     * @Route("/proyecto/{id}",
     *     methods={"POST"},
     *     name = "deleteProyectos")
     * @param int id
     * @return Response
     */
    public function deleteProyectoAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $proyecto = $this->getDoctrine()->getRepository(Proyecto::class);
        $deleteProyecto = $proyecto->findOneBy(['id'=>$id]);
        $manager->remove($deleteProyecto);
        $manager->flush();

        return API::send_data([]);
    }

}