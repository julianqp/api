<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Favorito;
use App\Service\API;

/**
 * Favorito controller.
 * @Route("/api", name="api_")
 */
class FavoritoController extends Controller
{
    /**
     * Lists all Favoritos.
     * @Route("/favoritos",
     *     methods = {"GET"},
     *     name = "getFavoritos")
     *
     * @return Response
     */
    public function getFavoritosAction()
    {
        $repository = $this->getDoctrine()->getRepository(Favorito::class);
        $favoritos = $repository->findall();
        return API::send_data($favoritos);
    }

    /**
     *  Info de una tarea.
     * @Route("/favoritos/{idColaborador}", methods={"GET"})
     * @param int idColaborador
     * @return Response
     */
    public function getFavorito($idColaborador)
    {
        $repository = $this->getDoctrine()->getRepository(Favorito::class);
        $favoritos = $repository->findBy(['idColaborador'=>$idColaborador]);
        return API::send_data($favoritos);
    }

    /**
     *  Info de una tarea.
     * @Route("/favoritos/{idColaborador}/{idProyecto}", methods={"GET"})
     * @param int idColaborador
     * @param int idProyecto
     * @return Response
     */
    public function getMiFavorito($idColaborador, $idProyecto)
    {
        $repository = $this->getDoctrine()->getRepository(Favorito::class);
        $favoritos = $repository->findOneBy(['idColaborador'=>$idColaborador,'idProyecto'=> $idProyecto ]);
        return API::send_data($favoritos);
    }

    /**
     *  Creating a new tarea.
     * @Route("/favoritos",
     *     methods={"POST"},
     *     name = "postFavorito")
     *
     * @return Response
     */
    public function postFavoritoAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $favorito = new Favorito();
        $favorito->setIdProyecto($data['idProyecto']);
        $favorito->setIdColaborador($data['idColaborador']);
        $manager->persist($favorito);
        $manager->flush();
        return API::send_data($favorito);
    }

    /**
     *  Deleting a tarea.
     * @Route("/favoritos/{idProyecto}/usuario/{idUsuario}",
     *     methods={"POST"},
     *     name = "deleteFavoritos")
     * @param int idProyecto
     * @param int idUsuario
     * @return Response
     */
    public function deleteFavoritoAction($idProyecto, $idUsuario)
    {
        $manager = $this->getDoctrine()->getManager();
        $favoritos = $this->getDoctrine()->getRepository(Favorito::class);
        $deleteFavorito = $favoritos->findOneBy(['idProyecto'=>$idProyecto, 'idColaborador' => $idUsuario]);

        $manager->remove($deleteFavorito);
        $manager->flush();
        return API::send_data([]);
    }
}