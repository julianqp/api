<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Tarea;
use App\Service\API;

/**
 * Tarea controller.
 * @Route("/api", name="api_")
 */
class TareaController extends Controller
{
    /**
     * Lists all Tareas.
     * @Route("/tareas",
     *     methods = {"GET"},
     *     name = "getTareas")
     *
     * @return Response
     */
    public function getTareasAction()
    {
        $repository = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repository->findall();
        return API::send_data($tareas);
    }

    /**
     *  Info de una tarea.
     * @Route("/tareas/{idProyecto}", methods={"GET"})
     * @param String idProyecto
     * @return Response
     */
    public function getTarea($idProyecto)
    {
        $repository = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repository->findBy(['idProyecto'=>$idProyecto]);
        return API::send_data($tareas);
    }

    /**
     *  Info de una tarea.
     * @Route("/tareas/colaborador/{idColaborador}", methods={"GET"})
     * @param String idColaborador
     * @return Response
     */
    public function getTareaColaboracion($idColaborador)
    {
        $repository = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repository->findBy(['idColaborador'=>$idColaborador]);

        return API::send_data($tareas);
    }

        /**
     *  Creating a new tarea.
     * @Route("/tareas",
     *     methods={"POST"},
     *     name = "postTarea")
     *
     * @return Response
     */
    public function postTareaAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $tarea = new Tarea();
        $tarea->setIdProyecto($data['idProyecto']);
        $tarea->setColaboracion(0);
        $tarea->setDescripcion($data['descripcion']);
        $tarea->setIdColaborador('');
        $manager->persist($tarea);
        $manager->flush();
        return API::send_data($tarea);
    }

    /**
     *  Creating a new tarea.
     * @Route("/tareas/apuntarse/{idTarea}",
     *     methods={"POST"},
     *     name = "apuntarseTarea")
     * @param int idTarea
     *
     * @return Response
     */
    public function postApuntarseTarea($idTarea, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Tarea::class);
        $tarea = $repository->findOneBy(['id'=>$idTarea]);

        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $tarea->setIdColaborador($data['idColaborador']);
        $tarea->setColaboracion($data['colaboracion']);
        $manager->merge($tarea);
        $manager->flush();
        return API::send_data($tarea);
    }

    /**
     *  Creating a new tarea.
     * @Route("/tareas/desapuntarse/{idTarea}",
     *     methods={"POST"},
     *     name = "desApuntarseTarea")
     * @param int idTarea
     *
     * @return Response
     */
    public function postDesApuntarseTarea($idTarea, Request $request)
    {

        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Tarea::class);
        $tarea = $repository->findOneBy(['idProyecto'=>$idTarea, 'idColaborador'=> $data['idColaborador']]);
        $tarea->setIdColaborador(' ');
        $tarea->setColaboracion(false);
        $manager->merge($tarea);
        $manager->flush();
        return API::send_data([]);
    }

    /**
     *  Deleting a tarea.
     * @Route("/tareas/{idProyecto}",
     *     methods={"DELETE"},
     *     name = "deleteTareas")
     * @param int idProyecto
     * @return Response
     */
    public function deleteTareaAction($idProyecto)
    {
        $manager = $this->getDoctrine()->getManager();
        $tareas = $this->getDoctrine()->getRepository(Tarea::class);
        $deleteTarea = $tareas->findBy(['idProyecto'=>$idProyecto]);
        $manager->remove($deleteTarea);
        $manager->flush();
        return API::send_data([]);
    }


}