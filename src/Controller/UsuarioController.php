<?php
namespace App\Controller;

use App\Entity\Tarea;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Usuario;
use App\Service\API;

/**
 * Usuario controller.
 * @Route("/api", name="api_")
 */
class UsuarioController extends Controller
{
    /**
     * Lists all Usuarios.
     * @Route("/usuarios",
     *     methods = {"GET"},
     *     name = "getUsuarios")
     *
     * @return Response
     */
    public function getUsuarioAction()
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
        $usuarios = $repository->findall();
        return API::send_data($usuarios);
    }

    /**
     *  Info de un usuario.
     * @Route("/usuarios/{id}", methods={"GET"})
     * @param String id
     * @return Response
     */
    public function getUsuario($id)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
//        $usuarios = $repository->findBy([],['apellidos' => 'ASC']);
        $usuarios = $repository->findOneBy(['id'=>$id]);

        return API::send_data($usuarios);
    }

    /**
     *  Creating a new user.
     * @Route("/usuarios",
     *     methods={"POST"},
     *     name = "postUsuarios")
     *
     * @return Response
     */
    public function postUsuarioAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $newUsuario = new Usuario();
        $newUsuario->setNombre($data['nombre']);
        $newUsuario->setApellidos($data['apellidos']);
        $newUsuario->setUsuario($data['usuario']);
        $newUsuario->setCorreo($data['correo']);
        $newUsuario->setPass($data['pass']);
        $newUsuario->setDescripcion('');
        $newUsuario->setActivo(0);
        $manager->persist($newUsuario);
        $manager->flush();

        return API::send_data($newUsuario);
    }

    /**
     *  editar a usuario.
     * @Route("/usuarios/{id}",
     *     methods={"POST"},
     *     name = "postEditarUsuarios")
     * @param int id
     *
     * @return Response
     */
    public function postEditarUsuario($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
        $newUsuario = $repository->findOneBy(['id'=>$id]);

        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $newUsuario->setDescripcion($data['descripcion']);
        $manager->merge($newUsuario);
        $manager->flush();

        return API::send_data($newUsuario);
    }

    /**
     *  Deleting a user.
     * @Route("/usuarios/{id}",
     *     methods={"DELETE"},
     *     name = "deleteUsuarios")
     * @param int id
     * @return Response
     */
    public function deleteUsuarioAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class);
        $deleteUsuario = $usuarios->findOneBy(['id'=>$id]);
        $manager->remove($deleteUsuario);
        $manager->flush();

        return API::send_data([]);
    }

     /**
     *  Deleting a user.
     * @Route("/login",
     *     methods={"POST"},
     *     name = "login")
     *
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class);
        $nombre = $data['usuario'];
        $pass = $data['pass'];
        $usuario = $usuarios->findOneBy(['usuario'=>$nombre, 'pass'=>$pass]);
        return API::send_data($usuario);
    }

}