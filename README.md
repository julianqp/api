# README #

### Crear base de datos

* php bin/console doctrine:database:create

### Crear tablas en base de datos

* php bin/console doctrine:schema:create

### Iniciar servidor

* php bin/console server:run api.local:8000